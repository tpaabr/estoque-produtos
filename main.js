/*
o.	Tarefa:
i.	Criar um sistema de gerenciamento de estoque usando os conceitos acima
ii.	O sistema deve:
1.	Permitir visualização do estoque
2.	Cadastro de produto
3.	Entrada de produto
4.	Saída de produto
5.	Contagem total do estoque
iii.Não é necessário persistir os dados
iv.	O front fica a critério de vocês
*/

let listaProdutos = [];

function Produto(nome, quantidade){
    this.nome = nome;
    this.quantidade = quantidade;
}

const $ = document

let butAdd = $.querySelector('#butAdicionar')

butAdd.addEventListener("click", () => {
    let nomeProduto = $.getElementById('nomeProduto').value;
    let quantInicialProduto = $.getElementById('quantidadeInicialProduto').value;

    let novoProduto = new Produto(nomeProduto, quantInicialProduto);

    listaProdutos.push(novoProduto);

    let htmlElement = $.createElement("LI");
    htmlElement.innerHTML = `<p>Nome: ${novoProduto.nome} || Quantidade: ${novoProduto.quantidade}</p>
     <button class="addQuant"> +1 </button>
     <button class="removeQuant">-1 </button>`;

    let htmlList = $.getElementById("listaProdutos");
    htmlList.appendChild(htmlElement);
})

let htmlList = $.getElementById("listaProdutos")
htmlList.addEventListener("click", (e) => {
    if (e.target && e.target.matches("li .addQuant")){
        let currTarget = e.target.parentNode;

        let amount = currTarget.innerText.match(/ [0-9]+/)
        let name = currTarget.innerText.match(/[a-z]+ /)

        amount = parseInt(amount)
        amount += 1

        currP = `Nome: ${name} || Quantidade: ${amount}`

        currTarget.innerHTML = `<p>${currP}</p>
        <button class="addQuant"> +1 </button>
        <button class="removeQuant"> -1 </button>`;

    }else if(e.target && e.target.matches("li .removeQuant")){
        let currTarget = e.target.parentNode;

        let amount = currTarget.innerText.match(/ [0-9]+/)
        let name = currTarget.innerText.match(/[a-z]+ /)

        amount = parseInt(amount)
        amount += -1

        let index = Array.prototype.indexOf.call(currTarget.parentNode.children, currTarget);
        if (amount === 0){
            currTarget.parentNode.removeChild(currTarget.parentNode.children[index])
            
        }

        currP = `Nome: ${name} || Quantidade: ${amount}`

        currTarget.innerHTML = `<p>${currP}</p>
        <button class="addQuant"> +1 </button>
        <button class="removeQuant"> -1 </button>`;
    }
})